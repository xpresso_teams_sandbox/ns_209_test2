#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

# Build the dependencies

set -e

install_alluxio(){
  cd ${ROOT_FOLDER}
  rm -rf ${ROOT_FOLDER}/alluxio-py
  git clone https://github.com/Alluxio/alluxio-py.git
  cd ${ROOT_FOLDER}/alluxio-py
  pip install -r requirements.txt
  pip install -e . --upgrade
  cd ${ROOT_FOLDER}
}

install_alluxio
